package lectures.lists.payroll;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Payroll {
	
	public static ArrayList<Employee> readData() {
		ArrayList<Employee> employees = new ArrayList<Employee>();
		
		try {
			File in = new File("src/lectures/lists/payroll/Employees.csv");
			Scanner input = new Scanner(in);
			
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					Employee e = null;
					
					String[] values = line.split(",");
					if (values.length == 3) {
						e = new SalaryEmployee(Integer.parseInt(values[0]), values[1], Double.parseDouble(values[2]));
					} else if (values.length == 4) {
						e = new HourlyEmployee(Integer.parseInt(values[0]), values[1], Double.parseDouble(values[2]), Double.parseDouble(values[3]));
					}
					
					if (e != null) {
						employees = addEmployee(employees, e);
					}
				} catch(Exception e) {
					System.out.println("Error reading row");
				}
			}
			
			input.close();
		} catch (Exception e) {
			System.out.println("Error reading file");
		}
		
		return employees;
	}
	
	public static Employee createEmployee(Scanner input) {
		Employee e = null;
		
		System.out.print("ID: ");
		int id = Integer.parseInt(input.nextLine());
		System.out.print("Name: ");
		String name = input.nextLine();
		System.out.print("Employee Type: ");
		String type = input.nextLine();
		if (type.equalsIgnoreCase("salary")) {
			System.out.print("Salary: ");
			double salary = Double.parseDouble(input.nextLine());
			e = new SalaryEmployee(id, name, salary);
		} else {
			System.out.print("Hourly Wages: ");
			double wage = Double.parseDouble(input.nextLine());
			System.out.print("Hours: ");
			double hours = Double.parseDouble(input.nextLine());
			e = new HourlyEmployee(id, name, wage, hours);
		}
		
		return e;
	}
	
	public static ArrayList<Employee> addEmployee(ArrayList<Employee> employees, Employee e) {		
		employees.add(e);
		
		return employees;
	}
	
	public static ArrayList<Employee> removeEmployee(Scanner input, ArrayList<Employee> employees) {
		
		ArrayList<Employee> possible = search(input, employees);
		
		if (possible.size() == 0) {
			System.out.println("Employee not found.");
		} else if (possible.size() == 1) {
			employees.remove(possible.get(0));
			System.out.println(possible.get(0).getName() + " has been removed.");
		} else {
			for (int i=0; i<possible.size(); i++) {
				System.out.println((i+1)+". "+possible.get(i).details());
			}
			System.out.println((possible.size()+1) + ". Exit");
			System.out.print("Choice: ");
			int index = possible.size() + 1;
			try {
				index = Integer.parseInt(input.nextLine());
			} catch (Exception e) {
				System.out.print("Not a valid choice. ");
			}
			
			if (index > 0 && index <= possible.size()) {
				employees.remove(possible.get(index-1));
				System.out.println(possible.get(index-1).getName() + " has been removed.");
			} else if (index == (possible.size() + 1)) {
				System.out.println("Returning to main menu.");
			} else {
				System.out.println("Not a valid choice. Returning to main menu.");
			}
		}
		
		return employees;
	}
	
	public static void lookupEmployee(Scanner input, ArrayList<Employee> employees) {
		ArrayList<Employee> searchResults = search(input, employees);
		
		for (Employee current : searchResults) {
			System.out.println(current.details());
		}
	}
	
	public static ArrayList<Employee> search(Scanner input, ArrayList<Employee> employees) {
		ArrayList<Employee> searchResults = new ArrayList<Employee>();
		
		System.out.print("Employee Name: ");
		String name = input.nextLine();
		
		for (Employee current : employees) {
			if (current.getName().equalsIgnoreCase(name)) {
				searchResults.add(current);
			}
		}
		
		return searchResults;
	}
	
	public static double payEmployees(ArrayList<Employee> employees) {
		double total = 0;
		for (Employee employee : employees) {
			double paid = employee.paid();
			System.out.println(employee.getName() + " - " + paid);
			total += paid;
		}
		
		return total;
	}
	
	public static void saveData(ArrayList<Employee> employees) {
		try {
			FileWriter out = new FileWriter("src/lectures/lists/payroll/Employees.csv");
			for (Employee employee : employees) {
				out.write(employee.toString() + "\n");
				out.flush();
			}
			out.close();
		} catch (Exception e) {
			
		}
	}

	public static void main(String[] args) {
		// Read in data (want)
		ArrayList<Employee> employees = readData();
		Scanner input = new Scanner(System.in);
		boolean done = false;
		
		do {
		// Menu (need)
			System.out.println();
			System.out.println("1. Create New Employee");
			System.out.println("2. Remove an Employee");
			System.out.println("3. Lookup an Employee");
			System.out.println("4. Pay Employees");
			System.out.println("5. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			System.out.println();
			switch (choice) {
				case "1":	// 1. Create Employee (need)
					employees = addEmployee(employees, createEmployee(input));
					break;
				case "2":	// 2. Remove Employee (want)
					employees = removeEmployee(input, employees);
					break;
				case "3":	// 3. Lookup employee
					lookupEmployee(input, employees);
					break;
				case "4":	// 4. Pay Employees (need)
					payEmployees(employees);
					break;
				case "5": // 5. Exit (need)
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I didn't understand that.");
			}
		} while (!done);
		// Save data (want)
		saveData(employees);
		
		System.out.println("Goodbye!");
	}

}
