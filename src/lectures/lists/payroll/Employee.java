package lectures.lists.payroll;

public abstract class Employee {

	private int id;
	protected String name;
	
	public Employee() {
		id = 0;
		name = "Jane Doe";
	}
	
	public Employee(int id, String name) {
		this.id = 0;
		setID(id);
		this.name = name;
	}
	
	public int getID() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setID(int id) {
		if (id > 0) {
			this.id = id;
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return id + "," + name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Employee)) {
			return false;
		}
		
		Employee e = (Employee)obj;
		if (id != e.getID()) {
			return false;
		} else if (!name.equals(e.getName())) {
			return false;
		}
		
		return true;
	}
	
	public abstract double paid();
	
	public String details() {
		String results = "";
		results += "ID: " + id + "\n";
		results += "Name: " + name;
		
		return results;
	}
	
}
