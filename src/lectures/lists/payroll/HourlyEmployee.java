package lectures.lists.payroll;

public class HourlyEmployee extends Employee {
	
	private double wage;
	private double hours;
	
	public HourlyEmployee() {
		super();
		wage = 0.0;
		hours = 0.0;
	}
	
	public HourlyEmployee(int id, String name, double wage, double hours) {
		super(id, name);
		setWage(wage);
		setHours(hours);
	}
	
	public double getWage() {
		return wage;
	}
	
	public double getHours() {
		return hours;
	}
	
	public void setWage(double wage) {
		if (wage > 0) {
			this.wage = wage;
		}
	}
	
	public void setHours(double hours) {
		if (hours >= 0) {
			this.hours = hours;
		}
	}
	
	public String toString() {
		return super.toString() + "," + wage + "," + hours;
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof HourlyEmployee)) {
			return false;
		}
		
		HourlyEmployee he = (HourlyEmployee)obj;
		if (!super.equals(he)) {
			return false;
		} else if (wage != he.getWage()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public double paid() {
		return this.hours * this.wage;
	}
	
	@Override
	public String details() {
		String results = super.details() + "\n";
		results += "HOURLY\n";
		results += "\tWage: $" + wage + "\n";
		results += "\tHours: " + hours;
		
		return results;
	}

}
