package lectures.lists;

public class StringList {

	private int size;
	private String[] data;
	public int next;
	
	public StringList() {
		size = 0;
		next = 0;
		data = new String[10];
	}
	
	public StringList(int capacity) {
		size = 0;
		next = 0;
		data = new String[capacity];
	}
	
	public int size() {
		return size;
	}
	
	public void add(String item) {
		if (size == data.length) {
			String[] temp = new String[size + 10];
			for (int i=0; i<size; i++) {
				temp[i] = data[i];
			}
			data = temp;
			temp = null;
		}
		
		data[size] = item;
		next = 0;
		size++;
	}
	
	public String get(int i) {
		if (i < 0) {
			return null;
		} else if (i >= size) {
			return null;
		}
		
		return data[i];
	}
	
	public String remove(int i) {
		if (i < 0) {
			return null;
		} else if (i >= size) {
			return null;
		}
		
		String value = data[i];
		for (int j=i; j<(size-1); j++) {
			data[j] = data[j+1];
		}
		size--;
		next = 0;
		
		return value;
	}
	
	public String remove(String item) {
		int i = indexOf(item);
		
		return remove(i);
	}
	
	public String removeAll(String item) {
		boolean done = false;
		
		do {
			int i = indexOf(item);
			
			if (i == -1) {
				done = true;
			} else {
				remove(i);
			}
		} while (!done);
		
		return item;
	}
	
	public int indexOf(String item) {
		int i = -1;
		for (int j=0; j<size&&i==-1; j++) {
			if (item.equals(data[j])) {
				i = j;
			}
		}
		
		return i;
	}
	
	public int lastIndexOf(String item) {
		int i = -1;
		for (int j=(size - 1); j>=0&&i==-1; j--) {
			if (item.equals(data[j])) {
				i = j;
			}
		}
		
		return i;
	}
	
	public boolean contains(String item) {
		return indexOf(item) != -1;
	}
	
	public boolean hasNext() {
		return next < size;
	}
	
	public String next() {
		if (!hasNext()) {
			return null;
		}
		
		String value = data[next];
		next++;
		return value;
	}
	
	public void reset() {
		next = 0;
	}
	
	public void clear() {
		size = 0;
		next = 0;
	}
	
}
