package lectures.lists;

public class ListApplication {

	public static void main(String[] args) {
		StringList list = new StringList();
		list.add("hello");
		list.add("again");
		list.add("goodbye");
		
		for (int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		list.remove(1);
		System.out.println();
		
		for (int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		list.remove("goodbye");
		System.out.println();
		
		for (int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		list.add("word");
		list.add("number");
		list.add("word");
		System.out.println();
		
		for (int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		list.removeAll("word");
		System.out.println();
		
		for (int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		System.out.println();
		System.out.println(list.indexOf("number"));
		
		while (list.hasNext()) {
			System.out.println(list.next());
		}
		
		list.clear();
		
		for (int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
	}

}
