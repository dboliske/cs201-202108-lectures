package lectures.repetition;

import java.util.Scanner;

public class ExampleThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		boolean done = false;
		
		while (!done) {
			System.out.print("Enter a letter between 'a' and 'd' (or 'Exit'): ");
			String value = input.nextLine().toLowerCase();
			switch(value) {
				case "a":
					System.out.println("Apple");
					break;
				case "b":
					System.out.println("Bat");
					break;
				case "c":
					System.out.println("Cat");
					break;
				case "d":
					System.out.println("Dog");
					break;
				case "exit":
					done = true;
					break;
				default:
					System.out.println("What!?");
			}
		}
		input.close();
		System.out.println("Goodbye");
	}

}
