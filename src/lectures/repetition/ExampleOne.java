package lectures.repetition;

import java.util.Scanner;

public class ExampleOne {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a grade (or -1 to exit): ");
		double grade = Double.parseDouble(input.nextLine());
		int counter = 0;
		double total = 0.0;
		while (grade >= 0) {
			counter++;
			total += grade;
			
			System.out.print("Enter another grade (or -1 to exit): ");
			grade = Double.parseDouble(input.nextLine());
		}
		input.close();
		
		System.out.print("Average: " + (total/counter));
	}

}
