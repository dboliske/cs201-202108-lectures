package lectures.repetition;

import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		boolean done = false;
		double prev = 0.0;
		boolean hasPrev = false;
		
		do {
			System.out.print("> ");
			
			String xString = input.next();
			if (!xString.equalsIgnoreCase("exit")) {
				String op = input.next();
				String yString = input.next();
				boolean badInput = false;
				
				double x = 0;
				if (xString.equals("%") && hasPrev) {
					x = prev;
				} else if (xString.equals("%")) {
					badInput = true;
				} else {
					x = Double.parseDouble(xString);
				}
				
				double y = 0;
				if (yString.equals("%") && hasPrev) {
					y = prev;
				} else if (yString.equals("%")) {
					badInput = true;
				} else {
					y = Double.parseDouble(yString);
				}
				
		//		System.out.println("X = " + x);
		//		System.out.println("Y = " + y);
		//		System.out.println("Operation: " + op);
				
				if (badInput) {
					System.out.println("Bad Input.");
				} else {
				
					switch (op) {
						case "+":
							prev = x + y;
							hasPrev = true;
							System.out.println(prev);
							break;
						case "-":
							prev = x - y;
							hasPrev = true;
							System.out.println(prev);
							break;
						case "*":
							prev = x * y;
							hasPrev = true;
							System.out.println(prev);
							break;
						case "/":
							prev = x / y;
							hasPrev = true;
							System.out.println(prev);
							break;
						case "\\":
							prev = y / x;
							hasPrev = true;
							System.out.println(prev);
							break;
						case "^":
							prev = Math.pow(x, y);
							hasPrev = true;
							System.out.println(prev);
							break;
						default:
							System.out.println("Bad input.");
					}
				}
			} else {
				done = true;
			}
			
		} while (!done);
		
		System.out.println("Goodbye");
		
		input.close();
		
	}

}
