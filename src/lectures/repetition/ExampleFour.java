package lectures.repetition;

import java.util.Scanner;

public class ExampleFour {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int counter = 0;
		double total = 0.0;
		double grade = 0.0;
		do {
			System.out.print("Enter a grade (or -1 to exit): ");
			grade = Double.parseDouble(input.nextLine());
			
			if (grade >= 0) {
				counter++;
				total+=grade;
			}
			
		} while (grade >= 0);
		input.close();
		
		System.out.print("Average: " + (total/counter));
	}

}
