package lectures.interfaces;

public class EmployeeApp {

	public static void main(String[] args) {
		Employee e1 = new Employee();
		Employee e2 = new Employee();
		e2.setId(1);
		System.out.println(e1.getId());
		System.out.println(e2.getId());
		System.out.println(e1.compareTo(e2));
		System.out.println();
		
		e1.print();
		
		ShopItem item = new ShopItem();
		item.print();
		
		Printable[] printing = new Printable[2];
		printing[0] = e1;
		printing[1] = item;
		
		for (int i=0; i<printing.length; i++) {
			printing[i].print();
		}
	}

}
