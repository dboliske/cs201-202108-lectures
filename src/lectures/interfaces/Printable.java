package lectures.interfaces;

public interface Printable {
	public void print();
}
