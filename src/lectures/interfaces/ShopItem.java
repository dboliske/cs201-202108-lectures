package lectures.interfaces;

public class ShopItem implements Printable {
	
	private String name;
	private double price;
	
	public ShopItem() {
		name = "Chips";
		price = 0.99;
	}
	
	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(double price) {
		if (price > 0) {
			this.price = price;
		}
	}
	
	@Override
	public String toString() {
		return "ShopItem [name=" + name + ", price=" + price + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ShopItem)) {
			return false;
		}
		
		ShopItem si = (ShopItem) obj;
		
		if (!(name.equals(si.getName()))) {
			return false;
		} else if (price != si.getPrice()) {
			return false;
		}
		
		return true;
	}

	@Override
	public void print() {
		System.out.println(name + " - $" + price);
	}

}
