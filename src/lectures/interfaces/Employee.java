package lectures.interfaces;

public class Employee implements Comparable<Employee>, Printable {

	private int id;
	private String name;
	
	public Employee() {
		this.id = 0;
		this.name = "Jane Doe";
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(int id) {
		if (id > 0) {
			this.id = id;
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Employee)) {
			return false;
		}
		
		Employee e = (Employee)obj;
		if (id != e.getId()) {
			return false;
		} else if (!name.equals(e.getName())) {
			return false;
		}
		
		return true;
	}

	@Override
	public int compareTo(Employee o) {
		return id - o.getId();
	}
	
	@Override
	public void print() {
		System.out.println("I'm an employee named " + name);
	}
	
}
