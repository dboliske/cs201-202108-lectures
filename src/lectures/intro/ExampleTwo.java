package lectures.intro;

public class ExampleTwo {

	public static void main(String[] args) {
		int x;
		int y;
		
		x = 42;
		y = 80;
		
		System.out.println(x + y);
		System.out.println(x * y);
		
		System.out.println(y - x);
		System.out.println(y / x);
		System.out.println(y % x);
		
		System.out.println((char)x);
		System.out.println((char)y);
	}

}
