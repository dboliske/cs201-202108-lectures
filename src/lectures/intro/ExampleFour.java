package lectures.intro;

public class ExampleFour {

	public static void main(String[] args) {
		String hello = "Hello";
		
		System.out.println(hello.length());
		System.out.println(hello.charAt(0));
		System.out.println(hello.substring(2));
		System.out.println(hello.substring(2, 4));
		System.out.println(hello.replace('l', '1'));
		System.out.println(hello.replaceAll("el","31"));
		
		System.out.println(hello.toUpperCase());
		System.out.println(hello.toLowerCase());
	}

}
