package lectures.generics;

import java.util.ArrayList;

public class SortedList<E extends Comparable<E>> {
	
	private int size;
	private ArrayList<E> data;
	
	public SortedList() {
		size = 0;
		data = new ArrayList<E>();
	}
	
	public SortedList(int initCap) {
		size = 0;
		data = new ArrayList<E>(initCap);
	}
	
	public int size() {
		return size;
	}
	
	public void add(E element) {
		int index = -1;
		for (int i=0; i<size&&index==-1; i++) {
			if (data.get(i).compareTo(element) > 0) {
				index = i;
			}
		}
		
		if (index == -1) {
			data.add(element);
		} else { 
			data.add(index, element);
		}
		size++;
	}
	
	public E remove(int i) {
		size--;
		return data.remove(i);
	}
	
	public E get(int i) {
		return data.get(i);
	}
	
	public int indexOf(E element) {
		int index = -1;
		int start = 0;
		int end = size;
		while (start < end) {
			int middle = (start + end) / 2;
			if (data.get(middle).equals(element)) {
				return middle;
			} else if (data.get(middle).compareTo(element) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		
		return index;
	}

}
