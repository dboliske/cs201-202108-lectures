package lectures.generics;

public class SortedListApp {

	public static void main(String[] args) {
		SortedList<Integer> intList = new SortedList<Integer>();
		intList.add(4);
		intList.add(2);
		intList.add(3);
		intList.add(1);
		intList.add(5);
		for (int i=0; i<intList.size(); i++) {
			System.out.println(intList.get(i));
		}
		
		for (int i=0; i<=6; i++) {
			int index = intList.indexOf(i);
			if (index == -1) {
				System.out.println("Not found");
			} else {
				System.out.println(i + "@" + index);
			}
		}
		
		SortedList<String> stringList = new SortedList<String>();
		stringList.add("dog");
		stringList.add("bat");
		stringList.add("cat");
		stringList.add("apple");
		stringList.add("egg");
		
		for (int i=0; i<stringList.size(); i++) {
			System.out.println(stringList.get(i));
		}
	}

}
