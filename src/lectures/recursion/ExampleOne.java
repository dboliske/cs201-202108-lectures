package lectures.recursion;

public class ExampleOne {
	
	public static void printRev(int[] data, int first, int last) {
		if (first == last) {
			System.out.println(data[last]);
			return;
		}
		
		System.out.println(data[last]);
		printRev(data, first, last - 1);
	}

	public static void main(String[] args) {
		
		int[] data = {74, 36, 87, 95};
		
		printRev(data, 0, 3);
		
	}

}
