package lectures.recursion;

import java.util.Scanner;

public class ExampleTwo {
	
	public static int log(int base, int n) {
		if (n == 1) {
			return 0;
		}
		
		return 1 + log(base, n/base);
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a value: ");
		int n = Integer.parseInt(input.nextLine());
		System.out.print("Enter a base: ");
		int base = Integer.parseInt(input.nextLine());
		
		System.out.println("Log/"+base+"(" + n + ") = " + log(base, n));
		
		input.close();
	}

}
