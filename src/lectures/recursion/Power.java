package lectures.recursion;

public class Power {
	
	public static double pow(int b, int n) {
		if (n == 0) {
			return 1;
		} else if (n < 0) {
			return 1.0 / pow(b, -n);
		}
		
		return b * pow(b, n-1);
	}

	public static void main(String[] args) {
		for (int i=-10; i<10; i++) {
			System.out.println("2^" + i + "=" + pow(2, i));
		}
	}

}
