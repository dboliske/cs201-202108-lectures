package lectures.recursion;

import java.util.Scanner;

public class Fib {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter an index: ");
		int n = Integer.parseInt(input.nextLine());
		
		int first = 0;
		int second = 1;
		for (int i=3; i<=n; i++) {
			int third = first + second;
			first = second;
			second = third;
		}
		
		System.out.println(second);
		
		input.close();
		
	}

}
