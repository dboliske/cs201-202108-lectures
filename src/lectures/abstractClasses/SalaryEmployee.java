package lectures.abstractClasses;

public class SalaryEmployee extends Employee {
	
	private double salary;
	
	public SalaryEmployee() {
		super();
		salary = 600.00;
	}
	
	public SalaryEmployee(int id, String name, double salary) {
		super(id, name);
		this.salary = 600.00;
		setSalary(salary);
	}
	
	public double getSalary() {
		return salary;
	}
	
	public void setSalary(double salary) {
		if (salary > 0) {
			this.salary = salary;
		}
	}
	
	public String toString() {
		return super.toString() + "," + salary;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SalaryEmployee)) {
			return false;
		}
		
		SalaryEmployee se = (SalaryEmployee)obj;
		if (!super.equals(se)) {
			return false;
		} else if (salary != se.getSalary()) {
			return false;
		}
		
		return true;
	}

	@Override
	public double paid() {
		return salary/52;
	}

}
