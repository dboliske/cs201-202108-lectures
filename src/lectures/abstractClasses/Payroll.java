package lectures.abstractClasses;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class Payroll {
	
	public static Employee[] readData() {
		Employee[] employees = new Employee[0];
		
		try {
			File in = new File("src/lectures/abstractClasses/Employees.csv");
			Scanner input = new Scanner(in);
			
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					Employee e = null;
					
					String[] values = line.split(",");
					if (values.length == 3) {
						e = new SalaryEmployee(Integer.parseInt(values[0]), values[1], Double.parseDouble(values[2]));
					} else if (values.length == 4) {
						e = new HourlyEmployee(Integer.parseInt(values[0]), values[1], Double.parseDouble(values[2]), Double.parseDouble(values[3]));
					}
					
					if (e != null) {
						employees = addEmployee(employees, e);
					}
				} catch(Exception e) {
					System.out.println("Error reading row");
				}
			}
			
			input.close();
		} catch (Exception e) {
			System.out.println("Error reading file");
		}
		
		return employees;
	}
	
	public static Employee createEmployee(Scanner input) {
		Employee e = null;
		
		System.out.print("ID: ");
		int id = Integer.parseInt(input.nextLine());
		System.out.print("Name: ");
		String name = input.nextLine();
		System.out.print("Employee Type: ");
		String type = input.nextLine();
		if (type.equalsIgnoreCase("salary")) {
			System.out.print("Salary: ");
			double salary = Double.parseDouble(input.nextLine());
			e = new SalaryEmployee(id, name, salary);
		} else {
			System.out.print("Hourly Wages: ");
			double wage = Double.parseDouble(input.nextLine());
			System.out.print("Hours: ");
			double hours = Double.parseDouble(input.nextLine());
			e = new HourlyEmployee(id, name, wage, hours);
		}
		
		return e;
	}
	
	public static Employee[] addEmployee(Employee[] employees, Employee e) {
		Employee[] temp = new Employee[employees.length + 1];
		for (int i=0; i<employees.length; i++) {
			temp[i] = employees[i];
		}
		employees = temp;
		
		employees[employees.length - 1] = e;
		
		return employees;
	}
	
	public static Employee removeEmployee(Scanner input) {
		
		return null;
	}
	
	public static double payEmployees(Employee[] employees) {
		double total = 0;
		for (int i=0; i<employees.length; i++) {
			double paid = employees[i].paid();
			System.out.println(employees[i].getName() + " - " + paid);
			total += paid;
		}
		return total;
	}
	
	public static void saveData(Employee[] employees) {
		try {
			FileWriter out = new FileWriter("src/lectures/abstractClasses/Employees.csv");
			for (int i=0; i<employees.length; i++) {
				out.write(employees[i].toString() + "\n");
				out.flush();
			}
			out.close();
		} catch (Exception e) {
			
		}
	}

	public static void main(String[] args) {
		// Read in data (want)
		Employee[] employees = readData();
		Scanner input = new Scanner(System.in);
		boolean done = false;
		
		do {
		// Menu (need)
			System.out.println();
			System.out.println("1. Create New Employee");
			System.out.println("2. Remove an Employee");
			System.out.println("3. Pay Employees");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			System.out.println();
			switch (choice) {
				case "1":	// 1. Create Employee (need)
					employees = addEmployee(employees, createEmployee(input));
					break;
				case "2":	// 2. Remove Employee (want)
					removeEmployee(input);
					break;
				case "3":	// 3. Pay Employees (need)
					payEmployees(employees);
					break;
				case "4": // 4. Exit (need)
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I didn't understand that.");
			}
		} while (!done);
		// Save data (want)
		saveData(employees);
		
		System.out.println("Goodbye!");
	}

}
