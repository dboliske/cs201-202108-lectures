package lectures.objects;

import java.util.Scanner;

public class DateTime {

	private int year;
	private int month; // 1-12
	private int day; // day of month, mostly between 1-31
	private int hour; // 24 hour
	private int minute; // 0-59
	private double second; // 0 to less than 60
	
	public DateTime() {
		year = 1970;
		month = 1;
		day = 1;
		hour = 0;
		minute = 0;
		second = 0.0;
	}
	
	public DateTime(int year, int month, int day) {
		setYear(year);
		this.month = 1;
		setMonth(month);
		this.day = 1;
		setDay(day);
		hour = 0;
		minute = 0;
		second = 0.0;
	}
	
	public DateTime(int year, int month, int day, int hour, int minute, double second) {
		setYear(year);
		this.month = 1;
		setMonth(month);
		this.day = 1;
		setDay(day);
		this.hour = 0;
		setHour(hour);
		this.minute = 0;
		setMinute(minute);
		this.second = 0.0;
		setSecond(second);
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public void setMonth(int month) {
		if (month >= 1 && month <= 12) {
			this.month = month;
		}
	}
	
	public void setDay(int day) {
		if (day >= 1 && day <= 31) {
			this.day = day;
		}
	}
	
	public void setHour(int hour) {
		if (hour >= 0 && hour <= 23) {
			this.hour = hour;
		}
	}
	
	public void setMinute(int minute) {
		if (minute >= 0 && minute <= 59) {
			this.minute = minute;
		}
	}
	
	public void setSecond(double second) {
		if (hour >= 0 && hour < 60) {
			this.second = second;
		}
	}
	
	public int getYear() {
		return year;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getDay() {
		return day;
	}
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public double getSecond() {
		return second;
	}
	
	public String toString() {
		return (year<0?"-":" ") + (Math.abs(year)<1000?"0":"") + (Math.abs(year)<100?"0":"") + (Math.abs(year)<10?"0":"") + Math.abs(year) + "/"
			+ (month<10?"0":"") + month + "/"
			+ (day<10?"0":"") + day + "T"
			+ (hour<10?"0":"") + hour + ":"
			+ (minute<10?"0":"") + minute + ":"
			+ (second<10?"0":"") + second;
	}
	
	public boolean equals(DateTime d) {
		if (year != d.getYear()) {
			return false;
		} else if (month != d.getMonth()) {
			return false;
		} else if (day != d.getDay()) {
			return false;
		} else if (hour != d.getHour()) {
			return false;
		} else if (minute != d.getMinute()) {
			return false;
		} else if (Math.abs(second - d.getSecond()) >= 0.001) {
			return false;
		}
		
		return true;
	}
	
	public boolean before(DateTime date) {
		if (year < date.getYear()) {
			return true;
		} else if (year == date.getYear()) {
			if (month < date.getMonth()) {
				return true;
			} else if (month == date.getMonth()) {
				if (day < date.getDay()) {
					return true;
				} else if (day == date.getDay()) {
					if (hour < date.getHour()) {
						return true;
					} else if (hour == date.getHour()) {
						if (minute < date.getMinute()) {
							return true;
						} else if (minute == date.getMinute()) {
							if (second < date.getSecond()) {
								return true;
							}
						}
					}
				}
			}
		}
		
		return false;
	}
	
	public boolean after(DateTime date) {
		return !equals(date) && !before(date);
	}
	
	public static DateTime constructDateTime() {
		return constructDateTime(new Scanner(System.in));
	}
	
	public static DateTime constructDateTime(Scanner input) {
		DateTime d = new DateTime();
		
		d.setYear(getInteger(input, "Enter Year: "));
		d.setMonth(getInteger(input, "Enter Month: "));
		d.setDay(getInteger(input, "Enter Day: "));
		d.setHour(getInteger(input, "Enter Hour: "));
		d.setMinute(getInteger(input, "Enter Minutes: "));
		d.setSecond(getDouble(input, "Enter Seconds: "));
		
		return d;
	}
	
	private static int getInteger(Scanner input, String prompt) {
		boolean done = false;
		int value = 0;
		
		do {
			System.out.print(prompt);
			String line = input.nextLine();
			try {
				value = Integer.parseInt(line);
				done = true;
			} catch (Exception e) {
				System.out.println("'" + line + "' is not a whole number.");
			}
		} while (!done);
		
		return value;
	}
	
	private static double getDouble(Scanner input, String prompt) {
		boolean done = false;
		double value = 0;
		
		do {
			System.out.print(prompt);
			String line = input.nextLine();
			try {
				value = Double.parseDouble(line);
				done = true;
			} catch (Exception e) {
				System.out.println("'" + line + "' is not a number.");
			}
		} while (!done);
		
		return value;
	}
	
}
