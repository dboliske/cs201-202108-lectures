package lectures.objects;

import java.util.Scanner;

public class ExceptionsTwo {
	
	public static double readDouble(Scanner input) {
		double value = 0;
		boolean done = false;
		
		do {
			System.out.print("Enter a double: ");
			String text = input.nextLine();
			try {
				value = Double.parseDouble(text);
				if (value >= 0) {
					done = true;
				} else {
					System.out.println(value + " needs to be a positive number. Please try again.");
				}
			} catch (Exception e) {
				System.out.println("'" + text + "' is not a number. Please try again.");
			}
		} while (!done);
		
		return value;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double value1 = readDouble(input);
		double value2 = readDouble(input);
		
		input.close();
		
		System.out.println(value1);
		System.out.println(value2);
		System.out.println(value1 * value2);
	}

}
