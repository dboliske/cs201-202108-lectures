package lectures.objects;

import java.util.Scanner;

public class DateTimeClient {

	public static void main(String[] args) {
		DateTime d = new DateTime();
		
		System.out.println(d.toString());
		
		d.setYear(1991);
		d.setMonth(11);
		d.setDay(26);
		d.setHour(13);
		d.setMinute(42);
		d.setSecond(3.14);

		System.out.println(d.toString());
		
		d.setYear(-10);
		d.setMonth(13);
		d.setDay(42);
		
		System.out.println(d);
		
		DateTime d2 = new DateTime(2021, 9, 27, 17, 37, 35);
		
		System.out.println(d.equals(d2));
		
		DateTime d3 = new DateTime(2021, 9, 27, 17, 37, 35);
		
		System.out.println(d2.equals(d3));
		
		System.out.println(d.before(d2));
		System.out.println(d2.before(d));
		System.out.println(d.after(d2));
		System.out.println(d2.after(d));
		
		System.out.println(d2.before(d3));
		System.out.println(d2.after(d3));
		
		Scanner input = new Scanner(System.in);
		
		DateTime d4 = DateTime.constructDateTime(input);
		
		System.out.println(d4);
		
		input.close();
		
	}

}
