package lectures.selection;

public class ExampleOne {

	public static void main(String[] args) {
		int x = 41;
		
		if (x % 2 == 0) {
			System.out.println("x is even");
		} else {
			System.out.println("x is odd");
		}
		
		System.out.println("Goodbye");
	}

}
