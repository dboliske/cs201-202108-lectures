package lectures.selection;

public class ExampleTwo {

	public static void main(String[] args) {
		char m = 'a';
		
		if (m == 'a') {
			System.out.println("Option a selected");
		} else if (m == 'b') {
			System.out.println("Option b selected");
		} else if (m == 'c') {
			System.out.println("Another option selected");
		} else {
			System.out.println("What!?");
		}
	}

}
