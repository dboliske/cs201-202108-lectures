package lectures.selection;

public class ExampleThree {

	public static void main(String[] args) {
		int x = 1;
		int y = 2;
		
		int min = 0;
		if (x < y) {
			min = x;
		} else {
			min = y;
		}
		
		System.out.println(min);
	}

}
