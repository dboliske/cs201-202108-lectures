package lectures.selection;

import java.util.Scanner;

public class ExampleSeven {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter yes or no: ");
		
		String in = input.nextLine();
		
		switch (in) {
			case "y": case "Y": case "yes": case "Yes":
				System.out.println("You entered yes.");
				break;
			default:
				System.out.println("You didn't enter yes.");
		}
		
		input.close();
	}

}
