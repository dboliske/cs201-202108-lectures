package lectures.selection;

import java.util.Scanner;

public class ExampleFive {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a letter between 'a' and 'd': ");
		
		String in = input.nextLine();
		char letter = in.toLowerCase().charAt(0);
		
		if (letter == 'a') {
			System.out.println("apple");
		} else if (letter == 'b') {
			System.out.println("bat");
		} else if (letter == 'c') {
			System.out.println("cat");
		} else if (letter == 'd') {
			System.out.println("dog");
		} else {
			System.out.println("I don't do that...");
		}
		
		input.close();
	}

}
