package lectures.selection;

import java.util.Scanner;

public class InputExample {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println(input.nextLine());
		
		input.close();
	}

}
