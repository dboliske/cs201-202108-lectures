package lectures.selection;

import java.util.Scanner;

public class ExampleEight {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter yes or no: ");
		
		String in = input.nextLine();
		
		if (in.equalsIgnoreCase("y") || in.equalsIgnoreCase("yes")) {
			System.out.println("You entered yes.");
		} else {
			System.out.println("You didn't entered yes.");
		}
		
		input.close();
	}

}
