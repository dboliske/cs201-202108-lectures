package lectures.selection;

import java.util.Scanner;

public class ExampleSix {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a letter between 'a' and 'd': ");
		
		String in = input.nextLine();
		
		switch (in) {
			case "a":
			case "A":
				System.out.println("apple");
				break;
			case "b":
			case "B":
				System.out.println("bat");
				break;
			case "c":
			case "C":
				System.out.println("cat");
				break;
			case "d":
			case "D":
				System.out.println("dog");
				break;
			default:
				System.out.println("I don't do that.");
		}
		
		input.close();
	}

}
