package lectures.exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ExampleOne {

	public static void main(String[] args) {
		
		double[] values = new double[10];
		int count = 0;
		
		File file = new File("src/lectures/exceptions/data.dat");
		try {
			Scanner input = new Scanner(file);
			
			while (input.hasNextLine()) {
				String line = input.nextLine();
				
				try {
					double value = Double.parseDouble(line);
					
					if (count == values.length) {
						double[] temp = new double[values.length + 10];
						for (int i=0; i<values.length; i++) {
							temp[i] = values[i];
						}
						values = temp;
					}
					
					values[count] = value;
					count++;
				} catch (Exception e) {
					// Do nothing
					System.out.println("Invalid value found: " + line);
					System.out.println("Continuing...");
				}
			}
			
			input.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		if (count < values.length) {
			double[] temp = new double[count];
			for (int i=0; i<temp.length; i++) {
				temp[i] = values[i];
			}
			
			values = temp;
		}
		
		double total = 0;
		for (int i=0; i<values.length; i++) {
			total += values[i];
		}
		
		if(values.length == 0) {
			System.out.println("No values found.");
		} else {
			System.out.println("Average: " + total/values.length);
		}
		
	}

}
