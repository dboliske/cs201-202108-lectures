package lectures.exceptions;

import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ExampleThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter your file's name: ");
		String name = input.nextLine();
		try {
			FileWriter file = new FileWriter("src/lectures/exceptions/" + name);
			boolean done = false;
			do {
				System.out.print("Enter a positive number to save to the file: ");
				String text = input.nextLine();
				if (text.equalsIgnoreCase("exit")) {
					done = true;
				} else {
					try {
						double value = Double.parseDouble(text);
						if (value < 0) {
							System.out.println("'" + text + "' needs to be positive");
						} else {
							file.write(value + "\n");
							file.flush();
						}
					} catch (InputMismatchException | NumberFormatException e) {
						System.out.println("'" + text + "' is not a number.");
					}
				}
			} while (!done);
			file.close();
		} catch (IOException e) {
			System.out.println("Error writing to file.");
		}
		
		input.close();
	}

}
