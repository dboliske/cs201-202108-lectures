package lectures.arrays;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExampleFive {

	public static void main(String[] args) throws IOException {
		String[] names = new String[5];
		double[] pay = new double[5];
		int count = 0;
		
		File employees = new File("src/lectures/arrays/employees.csv");
		Scanner file = new Scanner(employees);
		
		while (file.hasNextLine() && count < names.length) {
			String line = file.nextLine();
			String[] values = line.split(",");
			
			names[count] = values[0];
			pay[count] = Double.parseDouble(values[1]);
			count++;
		}
		
		file.close();
		
		int minAt = 0;
		for (int i=1; i<pay.length; i++) {
			if (pay[i] < pay[minAt]) {
				minAt = i;
			}
		}
		
		System.out.println(names[minAt] + " makes the least with $" + pay[minAt]);
	}

}
