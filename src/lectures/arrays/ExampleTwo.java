package lectures.arrays;

import java.util.Scanner;

public class ExampleTwo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int[] values = new int[5];
		for (int i=0; i<values.length; i++) {
			System.out.print("Enter an integer (" + (i+1) + "/" + values.length + "): ");
			values[i] = Integer.parseInt(input.nextLine());
		}
		
		input.close();
		
		int max = values[0];
		for (int i=1; i<values.length; i++) {
			if (values[i] > max) {
				max = values[i];
			}
		}
		
		System.out.println("Maximum: " + max);
	}

}
