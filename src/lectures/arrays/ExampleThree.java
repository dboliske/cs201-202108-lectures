package lectures.arrays;

public class ExampleThree {

	public static void main(String[] args) {
		String[][] matrix = new String[5][5];
		for (int i=0; i<matrix.length; i++) {
			matrix[i] = new String[]{"*", " ", " ", " ", "*"};
		}
		matrix[0] = new String[]{"*", "*", "*", "*", "*"};
		matrix[4] = matrix[0];
		
		for (int row=0; row<matrix.length; row++) {
			for (int column=0; column<matrix[row].length; column++) {
				System.out.print(matrix[row][column] + " ");
			}
			System.out.println();
		}
	}

}
