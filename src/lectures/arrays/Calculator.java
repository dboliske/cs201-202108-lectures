package lectures.arrays;

import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		boolean done = false;
		double[] prev = new double[0];
		
		do {
			System.out.print("> ");
			
			String xString = input.next();
			if (!xString.equalsIgnoreCase("exit")) {
				String op = input.next();
				String yString = input.next();
				boolean badInput = false;
				
				double x = 0;
				if (xString.equals("%") && prev.length > 0) {
					x = prev[prev.length - 1];
				} else if (xString.equals("%")) {
					badInput = true;
				} else if (xString.equals("%%") && prev.length > 1) {
					x = prev[prev.length - 2];
				} else if (xString.equals("%%")) {
					badInput = true;
				} else if (xString.charAt(0) == '%' && prev.length > 0) {
					int index = Integer.parseInt(xString.replace("%", ""));
					if (index >= 0 && index < prev.length) {
						x = prev[index];
					} else {
						badInput = true;
					}
				} else {
					x = Double.parseDouble(xString);
				}
				
				double y = 0;
				if (yString.equals("%") && prev.length > 0) {
					y = prev[prev.length - 1];
				} else if (yString.equals("%")) {
					badInput = true;
				} else if (yString.equals("%%") && prev.length > 1) {
					y = prev[prev.length - 2];
				} else if (yString.equals("%%")) {
					badInput = true;
				} else if (yString.charAt(0) == '%' && prev.length > 0) {
					int index = Integer.parseInt(yString.replace("%", ""));
					if (index >= 0 && index < prev.length) {
						y = prev[index];
					} else {
						badInput = true;
					}
				} else {
					y = Double.parseDouble(yString);
				}
				
		//		System.out.println("X = " + x);
		//		System.out.println("Y = " + y);
		//		System.out.println("Operation: " + op);
				
				if (badInput) {
					System.out.println("Bad Input.");
				} else {
					double results = 0;
					switch (op) {
						case "+":
							results = x + y;
							break;
						case "-":
							results = x - y;
							break;
						case "*":
							results = x * y;
							break;
						case "/":
							results = x / y;
							break;
						case "\\":
							results = y / x;
							break;
						case "^":
							results = Math.pow(x, y);
							break;
						default:
							System.out.println("Bad input.");
					}
					
					System.out.println("%" + prev.length + ": " + results);
					
					double[] temp = new double[prev.length+1];
					for (int i=0; i<prev.length; i++) {
						temp[i] = prev[i];
					}
					prev = temp;
					prev[prev.length-1] = results;
				}
			} else {
				done = true;
			}
			
		} while (!done);
		
		System.out.println("Goodbye");
		
		input.close();
		
	}

}
