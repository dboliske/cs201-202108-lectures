package lectures.arrays;

public class ExampleOne {

	public static void main(String[] args) {
		double[] values = {1.0, 2.0, 3.0, 4.0};
//		double[] values = new double[4];
//		values[0] = 1.0;
//		values[1] = 2.0;
//		values[2] = 3.0;
//		values[3] = 4.0;
		
		double total = 0.0;
		for (int i=0; i<values.length; i++) {
			total += values[i];
//			total = total + values[i];
		}
		
		System.out.println("Average: " + (total/values.length));
	}

}
