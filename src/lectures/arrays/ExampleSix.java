package lectures.arrays;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExampleSix {

	public static void main(String[] args) throws IOException {
		String[] names = new String[3];
		double[] pay = new double[3];
		int count = 0;
		
		File employees = new File("src/lectures/arrays/employees.csv");
		Scanner file = new Scanner(employees);
		
		while (file.hasNextLine()) {
			String line = file.nextLine();
			String[] values = line.split(",");
			
			if (count == names.length) {
				String[] temp1 = new String[names.length*2];
				double[] temp2 = new double[pay.length*2];
				for (int i=0; i<count; i++) {
					temp1[i] = names[i];
					temp2[i] = pay[i];
				}
				names = temp1;
				pay = temp2;
			}
			
			names[count] = values[0];
			pay[count] = Double.parseDouble(values[1]);
			count++;
		}
		
		file.close();
		
		if (count < names.length) {
			String[] temp1 = new String[count];
			double[] temp2 = new double[count];
			for (int i=0; i<count; i++) {
				temp1[i] = names[i];
				temp2[i] = pay[i];
			}
			names = temp1;
			pay = temp2;
		}
		
		int minAt = 0;
		for (int i=1; i<count; i++) {
			if (pay[i] < pay[minAt]) {
				minAt = i;
			}
		}
		
		System.out.println(names[minAt] + " makes the least with $" + pay[minAt]);
	}

}
