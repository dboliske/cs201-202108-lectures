package lectures.arrays;

public class ExampleSeven {

	public static void main(String[] args) {
		int[] a = {1, 2, 3};
		int[] b = {1, 2, 3};
		
		if (a.length == b.length) {
			boolean done = false;
			for (int i=0; i<a.length && !done; i++) {
				done = a[i] != b[i];
			}
			if (done) {
				System.out.println("Not Equal.");
			} else {
				System.out.println("Equal!");
			}
		} else {
			System.out.println("Not equal.");
		}
	}

}
