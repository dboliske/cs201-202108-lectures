# General Description

An application that represents a calendar with events. Should be able to add new events, remove existing ones, and edit existing ones.

## Event Description

Events should have the follow traits:

1. Start Time
2. End Time
3. Title

## Application Description

### Menu Description

The menu should look like the follow:

1. Add event
2. Edit event
3. Remove event
4. List events
5. Exit

### Needed Tools

* FileWriters and Scanners for using a file to persist data
* try/catches validate user input
* arrays to store events (will need to handle resizing)
* Event class - represents the event above
* DateTime class - for start and end dates of events
