package exercise.calendar;

public class ScratchProgram {

	public static void main(String[] args) {
		DayEvent e = new DayEvent();
		System.out.println(e.toString());
		System.out.println(e.toCSV());
		
		e.setStart(new Date(2021,10,4));
		System.out.println(e.toString());
		System.out.println(e.toCSV());
		
		e.setEnd(new Date(2021,10,5));
		e.setStart(new Date(2021,10,4));
		System.out.println(e.toString());
		System.out.println(e.toCSV());
	}

}
