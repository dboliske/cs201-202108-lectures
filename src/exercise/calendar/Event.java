package exercise.calendar;

public class Event {

	private String title;
	private DateTime start;
	private DateTime end;
	
	public Event() {
		title = "Event";
		start = new DateTime();
		end = new DateTime();
	}
	
	public Event(String title, DateTime start, DateTime end) {
		this.title = title;
		setStart(start);
		setEnd(end);
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setStart(DateTime start) {
		if (end != null && start.compareTo(end) < 0) {
			this.start = start;
		} else if (end == null) {
			this.start = start;
		}
	}
	
	public void setEnd(DateTime end) {
		if (start != null && start.compareTo(end) < 0) {
			this.end = end;
		} else if (start == null) {
			this.end = end;
		}
	}
	
	public String getTitle() {
		return title;
	}
	
	public DateTime getStart() {
		return start;
	}
	
	public DateTime getEnd() {
		return end;
	}
	
	public String toString() {
		return title + ": " + start.toString() + " - " + end.toString();
	}
	
	public String toCSV() {
		return title+","+start.toString()+","+end.toString();
	}
}
