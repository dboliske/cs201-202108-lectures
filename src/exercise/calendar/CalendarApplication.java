package exercise.calendar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class CalendarApplication {
	
	public static Event[] loadEvents() {
		Event[] cal = new Event[10];
		int count = 0;
		
		// read file...
		File in = new File("src/exercise/calendar/events.csv");
		try {
			Scanner input = new Scanner(in);
			
			while (input.hasNextLine()) {
				Event e = null;
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					if (values.length == 2) { // All day event
						e = new DayEvent(
							values[0], // title
							Date.parseDateTime(values[1]) // start
						);
					} else {
						e = new Event(
							values[0], // title
							DateTime.parseDateTime(values[1]), // start
							DateTime.parseDateTime(values[2]) // end
						);
					}
				} catch (Exception ex) {
					// if anything goes wrong with an individual event, we'll skip it
				}
				
				// Add event to the array if successfully read in
				if (e != null) {
					// needs to be resized
					if (count == cal.length) {
						Event[] temp = new Event[cal.length * 2];
						for (int i=0; i<cal.length; i++) {
							temp[i] = cal[i];
						}
						cal = temp;
					}
					
					cal[count] = e;
					count++;
				}
			}
			
			// Trim down to size if needed
			if (cal.length > count) {
				Event[] temp = new Event[count];
				for (int i=0; i<count; i++) {
					temp[i] = cal[i];
				}
				cal = temp;
			}
			
			input.close();
		} catch (FileNotFoundException e) {
			// if the file isn't found, that's fine, we just don't have any events yet
			cal = new Event[0];
		} catch (Exception e) {
			System.out.println("Error reading in current events. Proceeding with recovered data.");
		}
		
		return cal;
	}
	
	public static Event createEvent(Scanner input) {
		Event e;
		// Get event title
		System.out.print("Event Title: ");
		String title = input.nextLine();
		
		System.out.println("All Day Event: ");
		boolean allDay = Boolean.parseBoolean(input.nextLine());
		if (allDay) {
			e = new DayEvent();
		} else {
			e = new Event();
		}
		e.setTitle(title);
		// Get start time
		System.out.println("Start Time:");
		DateTime start;
		if (allDay) {
			start = Date.promptDateTime(input);
		} else {
			start = DateTime.promptDateTime(input);
		}
		
		if (!allDay) {
			// Get end time
			System.out.println("End Time:");
			DateTime end = DateTime.promptDateTime(input);
			e.setEnd(end);
			e.setStart(start);
		} else {
			e = new DayEvent(title, (Date)start);
		}
		
		
		return e;
	}
	
	public static Event[] addEvent(Event e, Event[] cal) {
		Event[] temp = new Event[cal.length+1];
		for (int i=0; i<cal.length; i++) {
			temp[i] = cal[i];
		}
		cal = temp;
		cal[cal.length - 1] = e;
		
		return cal;
	}
	
	public static int findEvent(Scanner input, Event[] cal) {
		System.out.print("Enter Title: ");
		String title = input.nextLine();
		
		for (int i=0; i<cal.length; i++) {
			if (cal[i].getTitle().equalsIgnoreCase(title)) {
				return i;
			}
		}
		
		return -1;
	}
	
	public static Event[] removeEvent(Event[] cal, int index) {
		
		for (int i=index; i<cal.length-1; i++) {
			cal[i] = cal[i+1];
		}
		
		Event[] temp = new Event[cal.length-1];
		for (int i=0; i<temp.length; i++) {
			temp[i] = cal[i];
		}
		cal = temp;
		
		return cal;
	}
	
	public static Event[] editEvent(Scanner input, Event[] cal, int index) {
		
		boolean done = false;
		
		do {
			System.out.println("Current: ");
			System.out.println("\tTitle: " + cal[index].getTitle());
			System.out.println("\tStart: " + cal[index].getStart());
			System.out.println("\tEnd: " + cal[index].getEnd());
			System.out.println();
			System.out.println("Menu:");
			System.out.println("1. Edit Title");
			System.out.println("2. Edit Start Date");
			System.out.println("3. Edit End Date");
			System.out.println("4. Done");
			System.out.print("Enter Choice: ");
			System.out.println();
			String choice = input.nextLine();
			switch(choice) {
				case "1": // Edit Title
					System.out.print("New Title: ");
					cal[index].setTitle(input.nextLine());
					System.out.println();
					break;
				case "2": // Edit start
					System.out.println("New Start Date:");
					cal[index].setStart(DateTime.promptDateTime(input));
					System.out.println();
					break;
				case "3": // Edit end
					System.out.println("New End Date:");
					cal[index].setEnd(DateTime.promptDateTime(input));
					System.out.println();
					break;
				case "4": // done
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I don't understand that.");
			}
		} while (!done);
		
		return cal;
	}
	
	public static void save(Event[] cal) {
		try {
			FileWriter out = new FileWriter("src/exercise/calendar/events.csv");
			for (int i=0; i<cal.length; i++) {
				out.write(cal[i].toCSV() + "\n");
				out.flush();
			}
			out.close();
		} catch (Exception e) {
			System.out.println("Failed to save events to file.");
		}
	}

	public static void main(String[] args) {
		// load/create Events array
		Event[] cal = loadEvents();
		
		Scanner input = new Scanner(System.in);
		boolean done = false;
		int index = -1; // declare head of time for use in switch
		
		do {
			// user menu
			System.out.println("Menu:");
			System.out.println("1. Add new event");
			System.out.println("2. Edit existing event");
			System.out.println("3. Remove event");
			System.out.println("4. List events");
			System.out.println("5. Exit");
			System.out.print("Enter Choice: ");
			String choice = input.nextLine();
			System.out.println();
			switch (choice) {
				case "1": // 1. add
					Event e = createEvent(input);
					cal = addEvent(e, cal);
					break;
				case "2": // 2. edit
					index = findEvent(input, cal);
					if (index == -1) {
						System.out.println("No event found.");
					} else {
						cal = editEvent(input, cal, index);
					}
					break;
				case "3": // 3. remove
					index = findEvent(input, cal);
					if (index == -1) {
						System.out.println("No event found.");
					} else {
						cal = removeEvent(cal, index);
					}
					break;
				case "4": // 4. list
					if (cal.length == 0) {
						System.out.println("No events");
					}
					for (int i=0; i<cal.length; i++) {
						System.out.println(cal[i].toString());
					}
					break;
				case "5": // 5. exit
					done = true;
					break;
				default:
					System.out.println("I'm sorry. I didn't understand that.");
			}
			System.out.println();
		} while (!done);
		
		input.close();
		// dump Events array to file
		save(cal);
		
		System.out.println("Closing...");
	}

}
