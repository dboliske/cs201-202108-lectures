package exercise.calendar;

import java.util.Scanner;

public class Date extends DateTime {

	public Date() {
		super();
	}
	
	public Date(int year, int month, int day) {
		super(year, month, day, 0, 0);
	}
	
	public void setHour(int hour) {
		super.setHour(0);
	}
	
	public void setMinute(int minute) {
		super.setMinute(0);
	}
	
	public String toString() {
		return getYear() + "-" + formatDigits(getMonth()) + "-" + formatDigits(getDay());
	}
	
	public static Date promptDateTime(Scanner input) {
		Date d = new Date();
		d.setYear(promptInteger(input, "Enter a year"));
		d.setMonth(promptInteger(input, "Enter a month", 1, 12));
		d.setDay(promptInteger(input, "Enter a day", 1, 31));
		
		return d;
	}
	
	public static Date parseDateTime(String s) {
		// Assumes format YYYY-MM-DD
		String[] date = s.split("-");
		
		return new Date(
			Integer.parseInt(date[0]),
			Integer.parseInt(date[1]),
			Integer.parseInt(date[2])
		);
	}
	
}
