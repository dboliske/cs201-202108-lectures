package exercise.calendar;

import java.util.Scanner;

public class DateTime {

	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	
	public DateTime() {
		year = 1970;
		month = 1;
		day = 1;
		hour = 0;
		minute = 0;
	}
	
	public DateTime(int year, int month, int day, int hour, int minute) {
		this();
		setYear(year);
		setMonth(month);
		setDay(day);
		setHour(hour);
		setMinute(minute);
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public void setMonth(int month) {
		if (month >= 1 && month <= 12) {
			this.month = month;
		}
	}
	
	public void setDay(int day) {
		if (day >= 1 && day <= 31) {
			this.day = day;
		}
	}
	
	public void setHour(int hour) {
		if (hour >= 0 && hour < 24) {
			this.hour = hour;
		}
	}
	
	public void setMinute(int minute) {
		if (minute >= 0 && minute < 60) {
			this.minute = minute;
		}
	}
	
	public int getYear() {
		return year;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getDay() {
		return day;
	}
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public String toString() {
		return year + "-" +
			formatDigits(month) + "-" +
			formatDigits(day) + "T" +
			formatDigits(hour) + ":" +
			formatDigits(minute);
	}
	
	protected String formatDigits(int value) {
		return (value<10?"0":"") + value;
	}
	
	public boolean equals(DateTime d) {
		return year == d.getYear() &&
			month == d.getMonth() &&
			day == d.getDay() &&
			hour == d.getHour() &&
			minute == d.getMinute();
	}
	
	public int compareTo(DateTime d) {
		if (year > d.getYear()) {
			return 1;
		} else if (year < d.getYear()) {
			return -1;
		}
		
		if (month > d.getMonth()) {
			return 1;
		} else if (month < d.getMonth()) {
			return -1;
		}
		
		if (day > d.getDay()) {
			return 1;
		} else if (day < d.getDay()) {
			return -1;
		}
		
		if (hour > d.getHour()) {
			return 1;
		} else if (hour < d.getHour()) {
			return -1;
		}
		
		if (minute > d.getMinute()) {
			return 1;
		} else if (minute < d.getMinute()) {
			return -1;
		}
		
		return 0;
	}
	
	public static DateTime promptDateTime(Scanner input) {
		DateTime d = new DateTime();
		d.setYear(promptInteger(input, "Enter a year"));
		d.setMonth(promptInteger(input, "Enter a month", 1, 12));
		d.setDay(promptInteger(input, "Enter a day", 1, 31));
		d.setHour(promptInteger(input, "Enter a hour", 0, 23));
		d.setMinute(promptInteger(input, "Enter a minute", 0, 59));
		
		return d;
	}
	
	public static int promptInteger(Scanner input, String prompt) {
		int value = 0;
		boolean done = false;
		do {
			System.out.print(prompt+": ");
			String in = input.nextLine();
			try {
				value = Integer.parseInt(in);
				done = true;
			} catch(Exception e) {
				System.out.println("'" + in + "' is not a valid integer.");
			}
		} while (!done);
		
		return value;
	}
	
	public static int promptInteger(Scanner input, String prompt, int low, int high) {
		int value = 0;
		boolean done = false;
		
		do {
			value = promptInteger(input, prompt);
			if (value < low || value > high) {
				System.out.println("'" + value + "' is outside the valid range.");
			} else {
				done = true;
			}
		} while(!done);
		
		return value;
	}
	
	public static DateTime parseDateTime(String s) {
		// Assumes format YYYY-MM-DDTHH:mm
		String[] dateAndTime = s.split("T");
		String date = dateAndTime[0];
		String time = dateAndTime[1];
		
		String[] dateValues = date.split("-");
		String[] timeValues = time.split(":");
		
		return new DateTime(
			Integer.parseInt(dateValues[0]),
			Integer.parseInt(dateValues[1]),
			Integer.parseInt(dateValues[2]),
			Integer.parseInt(timeValues[0]),
			Integer.parseInt(timeValues[1])
		);
	}
}
