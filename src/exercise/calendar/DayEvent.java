package exercise.calendar;

public class DayEvent extends Event {

	public DayEvent() {
		super();
		setEnd(new Date());
		setStart(new Date());
	}
	
	public DayEvent(String title, Date start) {
		Date end = new Date(start.getYear(), start.getMonth(), start.getDay());
		if (end.getDay() < 31) {
			end.setDay(end.getDay() + 1);
		} else {
			if (end.getMonth() < 12) {
				end.setMonth(end.getMonth() + 1);
				end.setDay(1);
			} else {
				end.setYear(end.getYear() + 1);
				end.setMonth(1);
				end.setDay(1);
			}
		}
		
		super.setTitle(title);
		super.setEnd(end);
		super.setStart(start);
	}
	
	public void setStart(DateTime start) {
		if (start instanceof Date) {
			super.setStart(start);
		} else if (start.getHour() == 0 && start.getMinute() == 0) {
			Date s = new Date(start.getYear(), start.getMonth(), start.getDay());
			super.setStart(s);
		}
	}
	
	public void setEnd(DateTime end) {
		if (end instanceof Date) {
			super.setEnd(end);
		} else if (end.getHour() == 0 && end.getMinute() == 0) {
			Date e = new Date(end.getYear(), end.getMonth(), end.getDay());
			super.setStart(e);
		}
	}
	
	public String toCSV() {
		return getTitle()+","+getStart().toString();
	}
	
}
