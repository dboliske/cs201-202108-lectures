package activity;

public class Trap {

	private int damage;
	private double hitChance;
	private Trigger trig;
	
	public Trap() {
		damage = 1;
		hitChance = 0.0;
		trig = Trigger.ENTRANCE;
	}
	
	public Trap(int damage, double hitChance, Trigger trig) {
		setDamage(damage);
		setHitChance(hitChance);
		this.trig = trig;
	}

	public int getDamage() {
		return damage;
	}

	public double getHitChance() {
		return hitChance;
	}
	
	public Trigger getTrigger() {
		return trig;
	}

	public void setDamage(int damage) {
		if (damage >= 0 && damage <= 10) {
			this.damage = damage;
		}
	}

	public void setHitChance(double hitChance) {
		if (hitChance >= 0 && hitChance <= 1) {
			this.hitChance = hitChance;
		}
	}
	
	public void setTrigger(Trigger trig) {
		this.trig = trig;
	}
	
	public void trigger(Character target) {
		double possibly = Math.random() + (target.getDex() / 10);
		if (possibly >= hitChance) {
			target.modifyHealth(-1 * damage);
		}
	}
}
