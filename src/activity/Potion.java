package activity;

public class Potion extends Item {
	
	public Potion() {
		super("health", 1);
	}
	
	public Potion(String name, int strength) {
		super(name, strength);
	}

	@Override
	public void use(Character user, Character target) {
		target.modifyHealth(super.getStrength());
	}

}
