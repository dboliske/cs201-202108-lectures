package activity;

public class Weapon extends Item {
	
	public Weapon() {
		super("dagger", 1);
	}
	
	public Weapon(String name, int strength) {
		super (name, strength);
	}

	@Override
	public void use(Character user, Character target) {
		int possibly = (int)(10 * Math.random());
		if (possibly <= user.getDex()) {
			int damage = -1 * (super.getStrength() + user.getStr());
			target.modifyHealth(damage);
		}
	}

}
