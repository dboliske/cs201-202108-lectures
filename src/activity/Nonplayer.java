package activity;

public class Nonplayer extends Character {

	private int hostility;
	
	public Nonplayer() {
		super();
		hostility = 10;
	}
	
	public Nonplayer(String name, int str, int dex, int wit, int mind, int health, int hostility, Weapon weapon) {
		super(name, str, dex, wit, mind, health, weapon);
		setHostility(hostility);
	}
	
	public int getHostility() {
		return hostility;
	}

	public void setHostility(int hostility) {
		if (validStat(hostility)) {
			this.hostility = hostility;
		}
	}
	
	public String toString() {
		String result = super.toString();
		result += "\n\tHOS: " + this.hostility;
		
		return result;
	}

	@Override
	public void attack(Character target) {
		int odds = (int)(10 * Math.random());
		if (odds > target.getDex()) {
			if (this.getWeapon() == null) {
				System.out.println(this.getName() + " punched " + target.getName() + "!");
				target.modifyHealth(-1 * this.getStr());
			} else {
				this.getWeapon().use(this, target);
				System.out.println(this.getName() + " attacked " + target.getName() + " with " + this.getWeapon().getName() + "!");
			}
		}
	}

	@Override
	public void talk(Character target) {
		int odds = (int)(10 * Math.random());
		if (odds > target.getMind()) {
			// target takes convincing
		}
	}
}
