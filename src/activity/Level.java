package activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Level {
	
	private Room entrance;
	private Room exit;
	private Room current;
	private ArrayList<Potion> potions;
	private ArrayList<Weapon> weapons;
	private ArrayList<Nonplayer> enemies;
	
	public Level() {
		entrance = null;
		exit = null;
		current = entrance;
		potions = new ArrayList<Potion>();
		weapons = new ArrayList<Weapon>();
		enemies = new ArrayList<Nonplayer>();
	}
	
	public Level(Room entrance, Room exit) {
		this.entrance = entrance;
		this.exit = exit;
		current = this.entrance;
		potions = new ArrayList<Potion>();
		weapons = new ArrayList<Weapon>();
		enemies = new ArrayList<Nonplayer>();
	}
	
	public Level(Room entrance, Room exit, Room current) {
		this.entrance = entrance;
		this.exit = exit;
		this.current = current;
		potions = new ArrayList<Potion>();
		weapons = new ArrayList<Weapon>();
		enemies = new ArrayList<Nonplayer>();
	}
	
	public void setEntrance(Room r) {
		this.entrance = r;
	}
	
	public void setExit(Room r) {
		this.exit = r;
	}
	
	public void setCurrent(Room r) {
		this.current = r;
	}
	
	public Room getEntrance() {
		return entrance;
	}
	
	public Room getExit() {
		return exit;
	}
	
	public void addPotion(Potion p) {
		potions.add(p);
	}
	
	public Potion getPotion(int i) {
		return potions.get(i);
	}
	
	public void addWeapon(Weapon w) {
		weapons.add(w);
	}
	
	public Weapon getWeapon(int i) {
		return weapons.get(i);
	}
	
	public void addEnemy(Nonplayer npc) {
		enemies.add(npc);
	}
	
	public Nonplayer getEnemy(int i) {
		return enemies.get(i);
	}
	
	public Room getCurrent() {
		return current;
	}
	
	public boolean isExit() {
		return this.exit.equals(this.current);
	}
	
	public boolean isExit(Room r) {
		return this.exit.equals(r);
	}
	
	public String toString() {
		ArrayList<Room> travelled = new ArrayList<Room>();
		return gatheredRooms(current, travelled, "", 0);
	}
	
	public String gatheredRooms(Room current, ArrayList<Room> travelled, String str, int depth) {
		String tabs = "";
		for (int i=0; i<depth; i++) {
			tabs += "  ";
		}
		String result = str + tabs + current.getName() + "\n";
		for (int i=0; i<current.doors(); i++) {
			Room neighbor = current.openDoor(i);
			if (!travelled.contains(neighbor)) {
				travelled.add(neighbor);
				result = gatheredRooms(neighbor, travelled, result, depth+1);
			}
		}
		return result;
	}
	
	public static Level buildLevel(String directory) {
		Level l = new Level();
		
		try {
			File potionFile = new File(directory + "/potions.csv");
			Scanner potionInput = new Scanner(potionFile);
			
			while (potionInput.hasNextLine()) {
				String line = potionInput.nextLine();
				String[] values = line.split(",");
				
				Potion p = null;
				try {
					p = new Potion(values[0], Integer.parseInt(values[1]));
				} catch (Exception e) {
					// failed for a line
				}
				
				if (p != null) {
					l.addPotion(p);
				}
			}
			
			potionInput.close();
		} catch (Exception e) {
			System.out.println("Error reading in potion data...");
		}
		
		try {
			File weaponFile = new File(directory + "/weapons.csv");
			Scanner weaponInput = new Scanner(weaponFile);
			
			while (weaponInput.hasNextLine()) {
				String line = weaponInput.nextLine();
				String[] values = line.split(",");
				
				Weapon w = null;
				try {
					w = new Weapon(values[0], Integer.parseInt(values[1]));
				} catch (Exception e) {
					// failed for a line
				}
				
				if (w != null) {
					l.addWeapon(w);
				}
			}
			
			weaponInput.close();
		} catch (Exception e) {
			System.out.println("Error reading in weapon data...");
		}
		
		try {
			File enemyFile = new File(directory + "/enemies.csv");
			Scanner enemyInput = new Scanner(enemyFile);
			
			while (enemyInput.hasNextLine()) {
				String line = enemyInput.nextLine();
				String[] values = line.split(",");
				
				Nonplayer npc = null;
				
				try {
					npc = new Nonplayer(values[0], Integer.parseInt(values[1]), Integer.parseInt(values[2]), Integer.parseInt(values[3]), Integer.parseInt(values[4]), Integer.parseInt(values[5]), Integer.parseInt(values[6]), null);
					String[] wIndex = values[7].split(":");
					Weapon w = null;
					if (wIndex.length == 1) {
						int i = (int)(wIndex.length * Math.random());
						i = i==wIndex.length?(i-1):i;
						w = l.getWeapon(Integer.parseInt(wIndex[i]) - 1);
					} else {
						w = l.getWeapon(Integer.parseInt(wIndex[0]) - 1);
					}
					npc.setWeapon(w);
					l.addEnemy(npc);
				} catch (Exception e) {
					System.out.println(e.toString());
				}
				
				if (npc != null) {
					l.addEnemy(npc);
				}
			}
			
			enemyInput.close();
		} catch (Exception e) {
			System.out.println("Error reading in weapon data...");
		}
		
		try {
			File roomFile = new File(directory + "/rooms.csv");
			Scanner roomInput = new Scanner(roomFile);
			
			ArrayList<String> lines = new ArrayList<String>();
			ArrayList<Room> rooms = new ArrayList<Room>();
			while (roomInput.hasNextLine()) {
				String line = roomInput.nextLine();
				lines.add(line);
				String[] values = line.split(",");
				
				Room r = new Room(values[0]);
				rooms.add(r);
			}
			roomInput.close();
			
			for (int i=0; i<lines.size(); i++) {
				String[] values = lines.get(i).split(",");
				if (values[1] != "") {
					String[] enemies = values[1].split(":");
					for (String e : enemies) {
						try {
							int index = Integer.parseInt(e);
							rooms.get(i).addEnemy(l.getEnemy(index-1));
						} catch (Exception exp) {
							// failed to parse index
						}
					}
				}
				if (values[2] != "") {
					String[] neighbors = values[2].split(":");
					for (String r : neighbors) {
						try {
							int index = Integer.parseInt(r);
							rooms.get(i).addRoom(rooms.get(index-1));
						} catch (Exception e) {
							// failed to parse index
						}
					}
				}
			}
			
			l.setEntrance(rooms.get(0));
			l.setExit(rooms.get(rooms.size()-1));
			l.setCurrent(rooms.get(0));
			
		} catch (Exception e) {
			System.out.println("Error reading in room data...");
		}
		
		return l;
	}

}
