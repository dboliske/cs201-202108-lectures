package activity;

public abstract class Character {

	protected String name;
	private int str;
	private int dex;
	private int wit;
	private int mind;
	private int health;
	private Weapon weapon;
	
	public Character() {
		name = "Link";
		str = 2;
		dex = 2;
		wit = 2;
		mind = 2;
		health = 10;
		weapon = new Weapon("sword", 2);
	}
	
	public Character(String name, int str, int dex, int wit, int mind, int health, Weapon weapon) {
		this.name = name;
		setStr(str);
		setDex(dex);
		setWit(wit);
		setMind(mind);
		setHealth(health);
		setWeapon(weapon);
	}

	public String getName() {
		return name;
	}

	public int getStr() {
		return str;
	}

	public int getDex() {
		return dex;
	}

	public int getWit() {
		return wit;
	}

	public int getMind() {
		return mind;
	}

	public int getHealth() {
		return health;
	}
	
	public Weapon getWeapon() {
		return weapon;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStr(int str) {
		if (validStat(str)) {
			this.str = str;
		}
	}

	public void setDex(int dex) {
		if (validStat(dex)) {
			this.dex = dex;
		}
	}

	public void setWit(int wit) {
		if (validStat(wit)) {
			this.wit = wit;
		}
	}

	public void setMind(int mind) {
		if (validStat(mind)) {
			this.mind = mind;
		}
	}
	
	protected boolean validStat(int stat) {
		return stat >= 0 && stat <= 10;
	}

	public void setHealth(int health) {
		if (validStat(health)) {
			this.health = health;
		}
	}
	
	public int modifyHealth(int diff) {
		int newHealth = this.health + diff;
		if (newHealth < 0) {
			this.setHealth(0);
		} else if (newHealth > 10) {
			this.setHealth(10);
		} else {
			this.setHealth(newHealth);
		}
		return newHealth;
	}
	
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}
	
	public String toString() {
		String result = this.name + "\n";
		result += "HP: " + this.health + "/10\n";
		result += "Weapon: " + this.weapon.getName() + "\n";
		result += "\tSTR: " + this.str + "\n";
		result += "\tDEX: " + this.dex + "\n";
		result += "\tWIT: " + this.wit + "\n";
		result += "\tMIN: " + this.mind;
		
		return result;
	}
	
	public abstract void attack(Character target);
	
	public abstract void talk(Character target);
	
}
