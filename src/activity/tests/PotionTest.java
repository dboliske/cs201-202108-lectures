package activity.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import activity.Potion;

public class PotionTest {

	@Test
	public void testPotionDefault() {
		Potion p = new Potion();
		assertEquals("health", p.getName());
		assertEquals(1, p.getStrength());
	}

	@Test
	public void testPotionNonDefault() {
		String name = "mana";
		int strength = 5;
		Potion p = new Potion(name, strength);
		
		assertEquals(name, p.getName());
		assertEquals(strength, p.getStrength());
	}

	@Test
	public void testSetName() {
		Potion p = new Potion();
		String name = "mana";
		p.setName(name);
		
		assertEquals(name, p.getName());
	}

	@Test
	public void testSetStrength() {
		Potion p = new Potion();
		
		int[] strengths = {5, 0, 10, -5, 15};
		
		for (int str : strengths) {
			p.setStrength(str);
			if (str < 0 || str > 10) {
				assertNotEquals(str, p.getStrength());
			} else {
				assertEquals(str, p.getStrength());
			}
		}
	}

}
