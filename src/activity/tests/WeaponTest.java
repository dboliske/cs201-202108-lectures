package activity.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import activity.Weapon;

public class WeaponTest {

	@Test
	public void testWeaponDefault() {
		Weapon w = new Weapon();
		assertEquals("dagger", w.getName());
		assertEquals(1, w.getStrength());
	}

	@Test
	public void testWeaponNonDefault() {
		String name = "sword";
		int strength = 5;
		Weapon w = new Weapon(name, strength);
		
		assertEquals(name, w.getName());
		assertEquals(strength, w.getStrength());
	}

	@Test
	public void testSetName() {
		Weapon w = new Weapon();
		String name = "sword";
		w.setName(name);
		
		assertEquals(name, w.getName());
	}

	@Test
	public void testSetStrength() {
		Weapon w = new Weapon();
		
		int[] strengths = {5, 0, 10, -5, 15};
		
		for (int str : strengths) {
			w.setStrength(str);
			if (str < 0 || str > 10) {
				assertNotEquals(str, w.getStrength());
			} else {
				assertEquals(str, w.getStrength());
			}
		}
	}

}
