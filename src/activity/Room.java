package activity;

import java.util.ArrayList;

public class Room {
	
	private String name;
	private ArrayList<Nonplayer> enemies;
	private ArrayList<Trap> traps;
	private ArrayList<Item> treasure;
	private ArrayList<Room> doors;
	
	public Room(String name) {
		this.name = name;
		enemies = new ArrayList<Nonplayer>();
		traps = new ArrayList<Trap>();
		treasure = new ArrayList<Item>();
		doors = new ArrayList<Room>();
	}
	
	public void addEnemy(Nonplayer e) {
		enemies.add(e);
	}
	
	public void addTrap(Trap t) {
		traps.add(t);
	}
	
	public void addTreasure(Item i) {
		treasure.add(i);
	}
	
	public void addRoom(Room r) {
		doors.add(r);
	}
	
	public int doors() {
		return doors.size();
	}
	
	public Room openDoor(int i) {
		if (i >= 0 && i < doors.size()) {
			return doors.get(i);
		}
		
		return this;
	}
	
	public String getName() {
		return name;
	}
	
	public int getNumberOfEnemies() {
		return enemies.size();
	}
	
	public void entered(Character target) {
		handleTraps(target, Trigger.ENTRANCE);
	}
	
	public void standing(Character target) {
		handleTraps(target, Trigger.DURING);
		handleAttacks(target);
	}
	
	public void exit(Character target) {
		handleTraps(target, Trigger.EXIT);
	}
	
	private void handleTraps(Character target, Trigger trig) {
		ArrayList<Trap> triggered = new ArrayList<Trap>();
		for (Trap t : traps) {
			if (t.getTrigger() == trig) {
				t.trigger(target);
				triggered.add(t);
			}
		}
		
		for (Trap t : triggered) {
			traps.remove(t);
		}
	}
	
	private void handleAttacks(Character target) {
		for (Nonplayer npc : enemies) {
			int hos = (int)(10 * Math.random());
			if (hos >= npc.getHostility()) {
				npc.attack(target);
			}
		}
	}
	
	public void cleanupDead() {
		ArrayList<Nonplayer> dead = new ArrayList<Nonplayer>();
		for (Nonplayer npc : enemies) {
			if (npc.getHealth() == 0) {
				dead.add(npc);
			}
		}
		
		for (Nonplayer npc : dead) {
			enemies.remove(npc);
		}
	}
	
	public void pickupItem(Player target, Item i) {
		if (treasure.remove(i)) {
			target.pickupItem(i);
		}
	}

}
