package activity;

public enum Trigger {
	ENTRANCE,
	DURING,
	EXIT;
}
