package activity;

public abstract class Item {

	protected String name;
	private int strength;
	
	public Item() {
		name = "widget";
		strength = 0;
	}
	
	public Item(String name, int strength) {
		this.name = name;
		setStrength(strength);
	}

	public String getName() {
		return name;
	}

	public int getStrength() {
		return strength;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStrength(int strength) {
		if (strength >= 0 && strength <= 10) {
			this.strength = strength;
		}
	}
	
	public String toString() {
		return this.name + ": " + this.strength;
	}
	
	public abstract void use(Character user, Character target);
	
}
