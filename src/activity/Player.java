package activity;

import java.util.ArrayList;

public class Player extends Character {
	
	private ArrayList<Item> inventory;
	
	public Player() {
		super();
		inventory = new ArrayList<Item>();
	}
	
	public Player(String name, int str, int dex, int wit, int mind, int health, Weapon weapon) {
		super(name, str, dex, wit, mind, health, weapon);
		inventory = new ArrayList<Item>();
	}
	
	public void pickupItem(Item i) {
		inventory.add(i);
	}
	
	public void useItem(Item i) {
		if (inventory.remove(i)) {
			i.use(this, this);
		}
	}
	
	public String toString() {
		String result = super.toString();
		
		return result;
	}

	@Override
	public void attack(Character target) {
		int odds = (int)(10 * Math.random());
		if (odds > target.getDex()) {
			this.getWeapon().use(this, target);
		}
	}

	@Override
	public void talk(Character target) {
		int odds = (int)(10 * Math.random());
		if (odds > target.getMind()) {
			Nonplayer npc = (Nonplayer)target;
			npc.setHostility(npc.getHostility() - 1);
		} else if (target instanceof Nonplayer) {
			Nonplayer npc = (Nonplayer)target;
			npc.setHostility(npc.getHostility() + 1);
		}
	}

}
