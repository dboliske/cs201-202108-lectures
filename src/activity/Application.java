package activity;

import java.util.ArrayList;
import java.util.Scanner;

public class Application {
	
	public static ArrayList<Level> buildLevels() {
		ArrayList<Level> levels = new ArrayList<Level>();
		levels.add(Level.buildLevel("src/activity/levels/level1"));
		
		return levels;
	}

	public static void main(String[] args) {
		Level first = buildLevels().get(0);
		System.out.println(first.toString());
		Scanner input = new Scanner(System.in);
		boolean done = false;
		Room prevRoom = first.getCurrent();
		Player you = new Player();
		
		do {
			
			System.out.println("Enter a door.");
			System.out.println(first.getCurrent().getName() + " - " + first.getCurrent().getNumberOfEnemies());
			for (int i=0; i<first.getCurrent().doors(); i++) {
				System.out.println((i+1) + ". " + first.getCurrent().openDoor(i).getName());
			}
			if (first.isExit()) {
				System.out.println((first.getCurrent().doors()+1) + ". Complete Level");
			}
			
			String choice = input.nextLine();
			int door = -1;
			try {
				door = Integer.parseInt(choice);
			} catch (Exception e) {
				System.out.println("'" + choice + "' is not a numbered option.");
			}
			
			if (door <= 0) {
				// not a valid option
				prevRoom = first.getCurrent();
			} else if (door >= 1 && door <= first.getCurrent().doors()) {
				first.setCurrent(first.getCurrent().openDoor(door - 1));
			} else if (door == (first.getCurrent().doors() + 1) && first.isExit()) {
				done = true;
			} else {
				System.out.println("'" + door + "' is not a valid option.");
				prevRoom = first.getCurrent();
			}
			
			if (prevRoom.equals(first.getCurrent())) {
				first.getCurrent().standing(you);
			}
			
		} while (!done && you.getHealth() > 0);
		
		input.close();
		
		if (you.getHealth() > 0) {
			System.out.println("You Win!");
		} else {
			System.out.println("You Lost!");
		}
	}

}
