# Dungeon Crawler

A game where a user plays a character proceeding through a sequence of rooms (sequence may not be linear). Rooms consist of challenges and contain rewards.

## User Interface

### Start of Program

Initially, describe character and describe entrance (first room). Maybe helpful prompts?

### Entering a Room

+ Describe room
+ Trigger traps (if any)
+ Trigger non-player characters
+ Reward treasure if available

### In Room Loop

+ Exit room
+ Interact with non-player characters
+ Pick up treasure
+ Use Item

### Exiting Room

+ Remaining NPCs act

### Final State

+ Find final boss
+ Defeat final boss
+ Exit becomes available


## Class Design

### Character Hierarchy

- Character
    - Name
    - Strength
    - Dexterity/Stealth?
    - Wit
    - Mind
    - Health
- Player Character
    - Items
    - Lives?
- Non-Player Characters
    - Feeling towards player?
    
### Rooms

- Exits
- Non-Player Characters
- Traps
- Treasures
