# CS 201

This repository will contain all examples written throughout the 2021 fall semester. There may not be much at the start, but it will grow with time (as we have already seen).

## Course Information

This course has additional resources and information available on the [course website](http://mypages.iit.edu/~dboliske).

## Questions

If you have any questions about the code presented here, please email me at [dboliske@hawk.iit.edu](mailto:dboliske@hawk.iit.edu) or message me on our [course Discord](https://discord.com/channels/876492986651922442/876492986651922445).